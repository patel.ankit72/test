var express = require('express');
var router = express.Router();

const moment = require('moment');

// middleware 
const { validate } = require('../middleware');

// validations
const { cardValidator } = require('../validations');

// models 
const { CardModel, EventModel, GuestModel } = require('../models');

router.post('/create', validate({ schema: cardValidator.create }), async function (req, res) {

    try {

        if (req.body.greet_date && !moment(req.body.greet_date).isValid()) {
            return res.status(402).json({ error: "invalid_greet_date", message: "Invalid Greet Date" });
        }

        const card = new CardModel(req.body);    
        const resp = await card.save();   
        
        res.send({ url: `${process.env.PUBLIC_URL}/ecard/${resp.slug}`, cardId: resp._id });
    } catch (err) {
        console.log("card.create.err => ", err);
        return res.status(400).json({ error: "invalid_requst", message: err.message });
    }
});

router.post('/generate', validate({ schema: cardValidator.cardGenerate }), async function (req, res) {

    try {

        // console.log("req.body ", req.body)

        if (req.body.greet_date && !moment(req.body.greet_date).isValid()) {
            return res.status(402).json({ error: "invalid_greet_date", message: "Invalid Greet Date" });
        }
        
        const card = new CardModel(req.body);    
        const cardResp = await card.save();

        if (req.body.event) {
            req.body.event.cardId = cardResp._id;
            const event = new EventModel(req.body.event);    
            const evntResp = await event.save();

            if (req.body.guests) {
                req.body.guests = req.body.guests.map((i) => Object.assign(i, { eventId: evntResp._id }) )
                const guestsResp = await GuestModel.insertMany(req.body.guests, { ordered: false }); 
            }
        }
         
  
        return res.send({ url: `${process.env.PUBLIC_URL}/ecard/${cardResp.slug}`, cardId: cardResp._id });

    } catch (err) {
        console.log("card.create.err => ", err);
        return res.status(400).json({ error: "invalid_requst", message: err.message });
    }
});


module.exports = router;
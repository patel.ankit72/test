var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

const moment = require('moment');

// middleware 
const { validate } = require('../middleware');

// validations
const { eventValidator, guestValidator } = require('../validations');

// models 
const { CardModel ,EventModel, GuestModel } = require('../models');
const helper = require('../libs/helper');

router.post('/create', validate({ schema: eventValidator.create }), async function (req, res) {

    try {

        const params = req.body;
        // if (req.body.greet_date && !moment(req.body.greet_date).isValid()) {
        //     return res.status(402).json({ error: "invalid_greet_date", message: "Invalid Greet Date" });
        // }
        var cardResp = await CardModel.findOne({ _id: params.cardId }, { _id:1 }).lean().exec();

        if (!cardResp) {
            return res.send({ error: "invalid_card", message: "Invalid cardId" })
        }

        var eventResp = await EventModel.findOne({ cardId: params.cardId }, { _id:1, slug: 1 }).lean().exec();

        if (eventResp) {
            return res.send({ 
                error: "event_exist", 
                message: "Event is already exists", 
                event: Object.assign(eventResp, { invitation_link: `${process.env.PUBLIC_URL}/invitation/${eventResp.slug}` })  })
        }

        // return res.send(eventResp)
         
        const event = new EventModel(req.body);    
        const resp = await event.save(); 
        
        return res.send({ eventId: resp._id, invitation_link: `${process.env.PUBLIC_URL}/invitation/${resp.slug}`  });

    } catch (err) {
        console.log("event.create.err => ", err);
        return res.status(400).json({ error: "invalid_requst", message: err.message });
    }    
});


router.post('/guest/invite', validate({ schema: guestValidator.guestInvite }), async function (req, res) {

    try {

        const params = req.body;

        if (helper.isEmptyOrZero(params.guests)) {
            return res.send({ error: "invalid_paramters", message: "Invalid Parameters"  })
        }

        var eventResp = await EventModel.findOne({ _id: params.eventId }, { _id:1, slug:1, allowTotalGuestsCount:1 }).lean().exec();

        if (!eventResp) {
            return res.send({ error: "invalid_event", message: "Invalid Event" })
        }

        if (eventResp.allowTotalGuestsCount > 0) {
            var filterGuest = params.guests.filter((i) => i.guestsCount > eventResp.allowTotalGuestsCount);

            if (!helper.isEmptyOrZero(filterGuest)) {
                return res.send({ error: "invalid_guest", message: `Total guest count allowed is ${eventResp.allowTotalGuestsCount}` })
            }
        }

        var list = params.guests.map((i) => Object.assign(i, { eventId: params.eventId }) )

        // reject if any one already exists 
        const guests = await GuestModel.insertMany(list, { ordered: true }); 

        const guestsResp = guests.map((i) => Object.assign({ 
            guestId: i._id, 
            name: i.name, 
            email: i.email,            
            uniqueInvitationLink: `${process.env.PUBLIC_URL}/invitation/${eventResp.slug}/${i._id}`
        }));

        return res.send(guestsResp);

    } catch (err) {
        // console.log("event.guest.invite.err => ", err);
        if (err.code === 11000) {
            return res.status(400).json({ error: "guest_exists", message: "One or more guest already invited" });
        }
        else {
            return res.status(400).json({ error: "invalid_requst", message: err.message });
        }
    }
});

router.post('/invitation/rsvp', async function (req, res) {

    try {     

        var params = req.body;
        
        if (!params.oiId || !params.name || (!params.email && !params.guestId)) {
            return res.status(400).json({ error: "invalid_request", message: "Invalid Request" });
        }

        if (params.email && !helper.emailIsValid(params.email)) {
            return res.status(400).json({ error: "invalid_request", message: "Invalid Request" });
        }

        if (!(Number(params.decision) >= 0 && Number(params.decision) <= 3)) {
            return res.status(400).json({ error: "invalid_request", message: "Invalid Request" });
        }        
        
        var data = {
            eventId: params.oiId,
            name: params.name,
            email: params.email,
            // phoneNumber: params.phoneNumber,
            guestsCount: Number(params.totalGuests) || 1,
            status: Number(params.decision),
        }
     
        // var guestId = params.guestId || new mongoose.Types.ObjectId();
        var query = {};

        if (data.email) {
            query.email = data.email
        }
        else {
            delete data.email
        }

        if (params.guestId) {
            query._id = params.guestId;
        }
          
        const resp = await GuestModel.findOneAndUpdate(query, { $set: data }, { upsert: true }).lean().exec() || {};

        return res.json(Object.assign(resp, { status: data.status }));

    } catch (err) { 
        console.log("event.invitation.rsvp.err ", err);
        return res.status(400).json({ error: "internal_server_error", message: err.message });
    }

});



module.exports = router;
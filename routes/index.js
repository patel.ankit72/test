var express = require('express');
var router = express.Router();

// api
router.use('/user', require('./user'));
router.use('/card', require('./card'));
router.use('/event', require('./event'));

module.exports = router;
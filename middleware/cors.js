const cors = require('cors');
var allowedOrigins = require('../statics/cors.json');

module.exports = function (req, res, next) {    
    var origin = req.headers.origin;

    if (allowedOrigins.indexOf(origin) > -1)
        res.setHeader('Access-Control-Allow-Origin', origin);
    else if(origin && origin.indexOf('chrome-extension://') > -1)
        res.setHeader('Access-Control-Allow-Origin', origin);

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, v-data, x-access-token, X-Forwarded-*");
    res.setHeader('Access-Control-Allow-Credentials', true);

    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};

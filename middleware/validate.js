const Joi = require('joi'); 
 
module.exports = (obj) => { 
    
    return (req, res, next) => {    

        if (!obj.schema) {
            throw new Error({ error: "INVALID_REQUEST", message: "Invalid request" })
        }
  
        const result = obj.schema.validate(req.body); 
        const { error } = result;     
 
        if (error) {
            res.status(400).json({ 
                error: "INVALID_REQUEST",
                message: 'Invalid request',                
                details: error.details
            }) 
        } else {
            next();
        }
    }
}

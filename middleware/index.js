
module.exports = {
    cors: require('./cors'),
    auth: require('./auth'),
    validate: require('./validate'),
}
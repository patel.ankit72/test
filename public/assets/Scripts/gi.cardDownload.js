function load_image(n, t) {
    var r = n.attr("src"),
        i;
    return r.indexOf("http") === -1 ? (t(n[0]), n[0]) : (i = document.createElement("img"), i.onload = function() {
        t(i)
    }, i.src = "/proxy?url=" + encodeURIComponent(r), i)
}

function render_image(n, t, i, r) {
    var u = document.createElement("canvas"),
        f;
    u.width = t;
    u.height = i;
    f = u.getContext("2d");
    f.fillStyle = "#FFFFFF";
    f.fillRect(0, 0, t, i);
    load_image(n, function(n) {
        f.drawImage(n, 0, 0, t, i);
        r(u)
    })
}

function get_rotated_image_helper(n, t, i) {
    var h;
    if (t === 0) i(n.toDataURL("image/jpeg"));
    else {
        var r = document.createElement("canvas"),
            s = r.getContext("2d"),
            u = n.width,
            f = n.height,
            e = 0,
            o = 0;
        switch (t) {
            case 90:
                u = n.height;
                f = n.width;
                o = n.height * -1;
                break;
            case 180:
                e = n.width * -1;
                o = n.height * -1;
                break;
            case 270:
                u = n.height;
                f = n.width;
                e = n.width * -1
        }
        r.setAttribute("width", u);
        r.setAttribute("height", f);
        s.rotate(t * Math.PI / 180);
        s.drawImage(n, e, o);        
        console.log("---rotated image drawn");
        h = r.toDataURL("image/jpeg");
        console.log("---rotated base64 image created");
        i(h)
    }
}

function download_card_as_pdf(n, t) {
    var h, i, d, nt, g, r, u, e, o, f, a, v, y, p;
    h = ["US", "USM", "CA", "MX", "DO", "PH", "CL"].indexOf(n) > -1 ? "letter" : "a4";
    console.log("=== CARD PDF EXPORT - started === " + h);
    var k = $(".preloader .front"),
        tt = $(".preloader .back"),
        it = $(".preloader .left"),
        rt = $(".preloader .right"),
        b = t === "landscape",
        c = 210,
        l = 297;
    h === "letter" && (c = 216, l = 279);
    d = !1;
    b ? (pdfDoc = new jsPDF("portrait", "mm", h), i = 180) : (pdfDoc = new jsPDF("landscape", "mm", h), d = !0, nt = c, c = l, l = nt, i = t === "square" ? 127 : 121);
    g = k[0].height / k[0].width;
    r = i * g;
    b ? (u = (c - i) / 2, e = (l - 2 * r) / 2, o = u, f = e + r, y = u, a = o, p = f, v = e) : (u = (c - 2 * i) / 2, o = u + i, f = (l - r) / 2, e = f, a = u, y = o, v = f, p = e);
    var s = 1500,
        w = parseInt(s * g);
    console.log("canvas size used for rendering: " + s + "x" + w);
    render_image(tt, s, w, function(n) {
        var t = 0;
        b && (t = 180);
        get_rotated_image_helper(n, t, function(n) {
            pdfDoc.addImage(n, "JPEG", u, e, i, r);
            render_image(k, s, w, function(n) {
                pdfDoc.addImage(n, "JPEG", o, f, i, r);
                pdfDoc.setDrawColor(170, 170, 170);
                pdfDoc.setLineWidth("0.05");
                pdfDoc.rect(o, f, i, r);
                pdfDoc.addPage();
                var t = 0;
                if (b) {
                    t = 180;
                    const n = v;
                    v = p;
                    p = n
                } else if (d) {
                    t = 180;
                    const n = a;
                    a = y;
                    y = n
                }
                render_image(it, s, w, function(n) {
                    get_rotated_image_helper(n, t, function(n) {
                        pdfDoc.addImage(n, "JPEG", a, v, i, r);
                        render_image(rt, s, w, function(n) {
                            get_rotated_image_helper(n, t, function(n) {
                                pdfDoc.addImage(n, "JPEG", y, p, i, r);
                                console.log("=== PDF EXPORT - DONE ===");
                                console.log("PDF direct download");
                                pdfDoc.save("Vinvite.pdf");
                                pdfDoc = null
                            })
                        })
                    })
                })
            })
        })
    })
}

const Joi = require('joi'); 

module.exports.guestInvite =  Joi.object({
 
    eventId: Joi.string().hex().length(24).required(),
    guests: Joi.array().items(Joi.object({ 
        name: Joi.string().max(30).required(), 
        email: Joi.string().max(50).required(),  
        guestsCount: Joi.number().greater(0).less(100).required(),  
    })).required()
    
}).unknown(false);


 
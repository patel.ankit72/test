
module.exports = {
    cardValidator: require('./card'),
    eventValidator: require('./event'),
    guestValidator: require('./guest'),
}
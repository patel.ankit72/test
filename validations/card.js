const Joi = require('joi'); 

const create = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().required(),
    files: Joi.array().items(Joi.object({ url: Joi.string().required(), place: Joi.string().required() })),
    type: Joi.string().required(),
    orientation: Joi.string().required(),
    is_rsvp: Joi.boolean().optional()
}).unknown(true);

module.exports.create =  create

const createEvent = require('./event').create
const guests = require('./guest').guestInvite


// module.exports.cardGenerate =  Joi.object({
    
//     event: createEvent,
    // guests: Joi.array().items(Joi.object({ 
    //     name: Joi.string().max(30).required(), 
    //     email: Joi.string().max(50).required(),  
    //     guestsCount: Joi.number().greater(0).less(100).required(),  
    // })).optional()
// }).unknown(false);

module.exports.cardGenerate =  Object.assign(create, { 
    event: createEvent,
    guests: Joi.array().items(Joi.object({ 
        name: Joi.string().max(30).required(), 
        email: Joi.string().max(50).required(),  
        guestsCount: Joi.number().greater(0).less(100).required(),  
    })).optional()
})
 
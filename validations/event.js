const Joi = require('joi'); 

module.exports.create =  Joi.object({
 
    cardId: Joi.string().hex().length(24).required(),

    eventName: Joi.string().max(50).required(),
    hostedBy: Joi.string().max(30).required(), 
    hostEmail: Joi.string().max(30).required(), 
    phoneNumber: Joi.string().max(15).optional(), 
    
    isVirtual: Joi.boolean().required(),
    virtService: Joi.string().max(30).optional(),
    virtUrl: Joi.string().max(200).optional(),
    virtDescription: Joi.string().max(500).optional(),

    locationName: Joi.string().max(50).optional(),
    location: Joi.string().max(100).optional(),
    location_coordinates: Joi.object({ properties: Joi.object({ type: Joi.string().required(), coordinates: Joi.array().required() }) }).optional(),
    
    whenDateHasTime: Joi.boolean().optional(),
    whenDate: Joi.date().optional(),
    whenTime: Joi.number().optional(),
    whenDateParsed:  Joi.date().optional(),

    endDateHasTime: Joi.boolean().optional(),
    endDate: Joi.date().optional(),
    endTime: Joi.number().optional(),
    endDateParsed: Joi.date().optional(),

    additionalInfo: Joi.string().max(500).optional(),

    showGuestList: Joi.boolean().optional(),
  
    registries: Joi.array().items(Joi.object({ name: Joi.string().max(30).optional(), url: Joi.string().max(200).optional() })).optional(),

    allowTotalGuestsCount: Joi.number().optional(),
    allowGuestsToAddComments: Joi.boolean().optional(),

    notifyMeOfEventActivity: Joi.boolean().optional(),
    notifyMeWhenGuestsRsvp: Joi.boolean().optional(),
    sendEventReminderDays: Joi.number().optional()

}).unknown(false);
 
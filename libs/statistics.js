var RequestStatics = [];
var EndpointStatics = {};


function getStatistics() {
    return {
        RequestStatics,
        EndpointStatics
    }
}

function endpointLogging(name) {

    if (!EndpointStatics[name]) {
      EndpointStatics[name] = 0;
    }
  
    EndpointStatics[name] += 1;
}
  

function logRequest(req, status) {

    try {
  
      const ipAddress = helpers.getUserIp(req);
      var requester = RequestStatics.find(o => o.ipAddress === ipAddress);
  
      const log = requester || { ipAddress: ipAddress, count: 0, failedCount: 0 }
      log.count += 1;
  
      if (!status) {
        log.failedCount += 1;
      }
    
      if (requester) {
        RequestStatics = RequestStatics.map(o => o.ipAddress === ipAddress ? log : o);
      }
      else {
        RequestStatics.push(log)
      }
       
  
    } catch (error) {
      console.log("logRequest", error)
    }
}
     

module.exports = {
    endpointLogging,
    getStatistics,
    logRequest
}
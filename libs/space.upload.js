"use strict";
//npm
var AWS = require('aws-sdk');
const stream = require('stream');
const request = require('request');

const s3 = new AWS.S3({
    endpoint: new AWS.Endpoint(process.env.SPACE_ENDPOINT),
    accessKeyId: process.env.SPACE_ACCESS_KEY_ID,
    secretAccessKey: process.env.SPACE_SECRET_ACCESS_KEY
});

function getExtension(url) {
    return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}

function getContentType(file) {

    var contentType = "";
    var ext = getExtension(file);

    switch (ext) {

        case ".png":
            contentType = "image/png";
            break;
        case ".webp":
            contentType = "image/webp";
            break; 
        case ".gif":
            contentType = "image/gif";
            break;
        case ".jpg":
            contentType = "image/jpeg";
            break;
        case ".jpeg":
            contentType = "image/jpeg";
            break;
        default:
            contentType = "application/octet-stream";
            break; 
    }

    return contentType;
}


function uploadByUrl(url, folderpath, filename) {
    
    try {

        if (!url) {
            return { error: "INVALID_PARAMETER" }
        }

        const pass = new stream.PassThrough();
        request(url).pipe(pass);
 
        var params = {
            Body: pass,
            Bucket: "ig-img",
            Key: `${folderpath}${filename}`,
            ACL:'public-read',
            ContentType: getContentType(filename)
        };
        
        s3.upload(params, function(err, data) {
            if (err) 
                console.log("space.upload.error", err);
        });

    } catch (err) {
        console.log("space.upload.error", err)
    }
}

module.exports = { uploadByUrl };

// (async () => {

//     uploadByUrl(
//         "https://instagram.fstv2-1.fna.fbcdn.net/v/t51.2885-15/e35/210568341_230969688861143_1833052658752245042_n.jpg?_nc_ht=instagram.fstv2-1.fna.fbcdn.net&_nc_cat=111&_nc_ohc=bjgUuirp8yYAX9jOmOj&edm=AIQHJ4wBAAAA&ccb=7-4&oh=1b668e6442fe4ec00e9d0ca1f464b268&oe=6122F067&_nc_sid=7b02f1",
//         "ig/profile/",
//         "test5.png"
//     );
 
// })()
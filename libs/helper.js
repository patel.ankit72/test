const URL = require('url');
const uuid = require('uuid');
const _ = require("lodash");
const fs = require('fs');
const PATH = require('path');
var moment = require('moment');

module.exports = {

    isObject: function(a) {
        return (!!a) && (a.constructor === Object);
    },
      
    isArray: function(a) {
        return (!!a) && (a.constructor === Array);
    },
      
    utils: function (promise) {
      return promise
        .then(data => ({ data, error: null }))
        .catch(error => ({ error, data: null }))
    },

    getUserIp: function (req) {

      var ipAddress = "";
      var forwardedIpsStr = req.header('x-forwarded-for');

      if (forwardedIpsStr) {
          var forwardedIps = forwardedIpsStr.split(',');
          ipAddress = forwardedIps[0];
      }

      if (!ipAddress && req.connection)
          ipAddress = req.connection.remoteAddress;

      return ipAddress;
    },

    safelyParseJSON: function(json) {

      var parsed

      try {
        parsed = JSON.parse(json)
      } catch (e) { }

      return parsed;
    },

    isEmptyOrZero : function (obj) {

      if(!obj)
        return true;

      else if(obj && obj.length == 0)
        return true;

      else
        return false;
    },

    getURL: function (req) {

        if (req) {
            return URL.parse(URL.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: req.originalUrl,
            }));
        }

        return {};
    },

    generateUUID: function(){
       return uuid.v4();
    },
 
    emptyIsValid: function (value) {
        var isEmpty;

        if (typeof value === "undefined")
            isEmpty = true;
        else if (value.trim() === null || value.trim() === '')
            isEmpty = true;
        else
            isEmpty = false;

        return isEmpty;
    },

    emailIsValid: function (value) {

        var isValidEmail = false;
        var emailPattern = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

        var emptyString = module.exports.emptyIsValid(value);

        if(emptyString)
            isValidEmail = false;
        else if(!value.toString().match(emailPattern))
            isValidEmail = false;
        else
            isValidEmail = true;

        return isValidEmail;
    },

    daysAfterMonth: function (month)
    {
        var currentDate = new Date();
        var lastDate = new Date().setMonth(new Date().getMonth() + month);
        var diffDays = parseInt((lastDate - currentDate) / (1000 * 60 * 60 * 24));
        return (diffDays)
    },

    timeSince: function (date) {

        var seconds = Math.floor((new Date() - date) / 1000);
        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        if (Math.floor(seconds) >= 30) {
            return Math.floor(seconds) + " seconds ago";
        }

        return "Just now";
    },

    nFormatter: function (num, digits) {

        //Number to Milion or k Converter
        var si = [
        { value: 1E18, symbol: "E" },
        { value: 1E15, symbol: "P" },
        { value: 1E12, symbol: "T" },
        { value: 1E9, symbol: "G" },
        { value: 1E6, symbol: "M" },
        { value: 1E3, symbol: "k" }
        ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;

        for (i = 0; i < si.length; i++)
            if (num >= si[i].value)
                return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;

        return num.toFixed(digits).replace(rx, "$1");
    },

    capitalizeFirstLetter: function (string) {
        return string ? string.charAt(0).toUpperCase() + string.slice(1) : '';
    },

    daysAgo: function (lastDate) {

        var oneDay = 24 * 60 * 60 * 1000;

        var firstDate = new Date();
        var secondDate = new Date(lastDate);

        var days = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime()) / (oneDay)));
        return days;
    },

    CreateToken: function (text, size) {
        text = text || "";
        size = size || 14;
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < size; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    randomIntFromInterval: function (min, max) {
        var randomize = JSON.parse(process.env.randomize);
        return randomize ? Math.floor(Math.random() * (max - min + 1) + min) : 1;
    },

    randomNumber: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    isValidDomain: function(val){
      return (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/.test(val))
    },

    isValidPatternURL: function (value) {
      if(!value) return false;
      var regex = /^(?:http(s)?:\/\/)?[*\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      var result = new RegExp(regex, 'gim').exec(value.trim());
      return result ? true : false;
    },

    isValidUserName: function (value) {
        if(!value) return false;
        const check = /^[a-z0-9_\.]+$/.exec(value.toLowerCase());
        return !!check;
    },

    _isObject:function(e){
        try{return _.isObject(JSON.parse(e))}catch(err){return false};
    },

    validURL:  function(url){
        if(!url) return false;
        var urlRegex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
        var result = new RegExp(urlRegex, 'gim').exec(url.trim());
        return result ? true : false;
    },

    hexToRgb: (hex) => {
        if(!(/^#([0-9A-F]{3}){1,2}$/i.test(hex))) return module.exports.rgbStringToObject(hex);
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
          return r + r + g + g + b + b;
        });      
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16)
        } : null;
    },

    removeFile: (filePath)=>{
        try {
            fs.unlinkSync(filePath);
        } catch(error){}
    },    

    checkBlockedList: (value) => {

        var isBlocked = false;

        for (let index = 0; index < blocked_list.length; index++) {
            const item = blocked_list[index];
            
            if (item.type === "email_provider" && value.indexOf(item.value) > -1) {
                isBlocked = true;
                break;
            }

        }

        return isBlocked;        
    },

    unicodeToChar: (text) => {
        return text.replace(/\\u[\dA-F]{4}/gi, 
               function (match) {
                    return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
               });
    },

    sleep: function(ms) {
        return new Promise((resolve) => {
          setTimeout(resolve, ms);
        });
    }      
};

'use strict';

const rateLimit = require("express-rate-limit");

//helpers
var helpers = require('../libs/helper');

//statics
var whitelisted_ip = require('../statics/whitelisted_ip.json');
var third_party_list = require('../statics/third_party_list.json');

function isWhitelisted(req, res) {
  var ipAddress = helpers.getUserIp(req);
  return  whitelisted_ip.filter((i) => { return i.ip == ipAddress })[0] ? true : false;
}

function keyGenerator(req, res) {
  var api_key =  req.query.api_key || req.headers["x-api-key"] || (req.cookies && req.cookies.api_key);  
  
  return api_key || helpers.getUserIp(req);
}

function keyGeneratorV2(req, res) {
  var api_key =  req.query.api_key || req.headers["x-api-key"] || (req.cookies && req.cookies.api_key);  
  return api_key || req.AuthUser.Id;
}

function setMaxByKey (req, res) {
  var api_key =  req.query.api_key || req.headers["x-api-key"] || (req.cookies && req.cookies.api_key);  
 
  if (!api_key) {
    return 0;
  }
  
  var clientInfo = third_party_list.find(o => o.api_key === api_key);

  // to-do: verify key expire date
  if (!clientInfo) {
    return 0;
  }

  return clientInfo.max || 0
}

module.exports.testLimiter = rateLimit({
  windowMs: 1 * 60 * 1000,    
  message: { error: "rate-limit", message: "Too many attempt, Please try again after few minutes." },
  keyGenerator: keyGenerator,
  max: setMaxByKey
});


module.exports.createUserLimiter = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 5,
  message: { error: "rate-limit", message: "Too many attempt, Please try again after few minutes." }
});

var mongoose = require('mongoose');

const helper = require('../libs/helper')
 
var schema = mongoose.Schema({   
    createdBy: { type: mongoose.Types.ObjectId, default: null },
    slug: { type: String, require: true, index: true, unique: true },
    name: { type: String, default: null },
    description: { type: String, default: null },
    type: { type: String, default: "flat", enum : ['flat', 'fold'] },
    content_type: { type: String, default: "image", enum: ["image", "video"] },
    orientation: { type: String, default: "portrait", enum: ['portrait', 'landscape'] },
    files: [{        
        url: { type: String, default: null },
        filename: { type: String, default: null },
        file_type: { type: String },
        place: { type: String, default: "front", enum : ['front','back', 'left', 'right'] }
    }],    
    hits: { type: Number, default: 0 },
    is_published: { type: Boolean, default: true },
    greet_date: { type: Date, default: null },
    datetime: {
        type: Date, 
        default: new Date()
    },
    is_rsvp: { type: Boolean, default: false }
},
{ 
    versionKey: false,
    timestamps: false,
    strict: false
});

schema.pre('save', function(next) {    
    if (!this.slug) {
        // check here for duplicate
       this.slug = helper.CreateToken("", 16);
    }  
    next();
});

var CardModel = mongoose.model('card', schema)
// CardModel.createIndexes();

module.exports = CardModel;


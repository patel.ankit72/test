var mongoose = require('mongoose');

const helper = require('../libs/helper')
 
var schema = mongoose.Schema({   
    createdBy: { type: mongoose.Types.ObjectId, default: null },    
    cardId: { type: mongoose.Types.ObjectId, require: true, unique: true, index: true },
    slug: { type: String, require: true, index: true, unique: true },

    eventName: { type: String, maxLength: 50, default: null },
    hostedBy: { type: String, maxLength: 30, default: null }, 
    hostEmail: { type: String, maxLength: 30, lowercase: true, trim: true, default: null, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'] }, 
    phoneNumber: { type: String, maxLength: 15, default: null }, 
    
    isVirtual: { type: Boolean, default: false },
    virtService: { type: String, maxLength: 30, default: null },
    virtUrl: { type: String, maxLength: 200, default: null },
    virtDescription: { type: String, maxLength: 300, default: null },

    locationName: { type: String, maxLength: 50, default: null },
    location: { type: String, maxLength: 200, default: null },
    location_coordinates: {
        type: Object,
        properties: {
          type: { type: String, enum: 'Point', default: 'Point' },
          coordinates: { type: [Number], default: [0, 0] }
        }
    },

    whenDateHasTime: { type: Boolean, default: true },
    whenDate: { type: Date, default: null },
    whenTime: { type: Number, default: null },
    whenDateParsed:  { type: Date, default: null },

    endDateHasTime: { type: Boolean, default: true },
    endDate: { type: Date, default: null },
    endTime: { type: Number, default: null },
    endDateParsed:  { type: Date, default: null },

    additionalInfo: { type: String, maxLength: 500, default: null },

    showGuestList: { type: Boolean, default: true },

    registries: [{
        name: { type: String, maxLength: 50, default: null },
        url: { type: String, maxLength: 200, default: null },
    }],

    allowTotalGuestsCount: { type: Number, default: 1 },
    allowGuestsToAddComments: { type: Boolean, default: true },

    notifyMeOfEventActivity: { type: Boolean, default: true },
    notifyMeWhenGuestsRsvp: { type: Boolean, default: true },
    sendEventReminderDays: { type: Number, default: 1 }
},
{ 
    versionKey: false,
    timestamps: true,
    strict: false
});

schema.pre('save', function(next) {    
    if (!this.slug) {
        // check here for duplicate
       this.slug = helper.CreateToken("", 16);
    }  
    next();
});

var EventModel = mongoose.model('event', schema)
EventModel.createIndexes();

module.exports = EventModel;


var mongoose = require('mongoose');
 
var schema = mongoose.Schema({   
    first_name: {
        type: String,
        trim: true,
        required: true,
    }, 
    last_name: {
        type: String,
        trim: true,
        required: true,
    },
    email: {
        type: String, default: null,
        trim: true,
        required: true,
        message: "Email address is required"
    },
    datetime: {
        type: Date, 
        default: new Date()
    }
},
{ 
    versionKey: false,
    timestamps: false,
    strict: false
});

var UserModel = mongoose.model('user', schema)

module.exports = UserModel;

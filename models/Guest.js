var mongoose = require('mongoose');

const helper = require('../libs/helper')
 
var schema = mongoose.Schema({   
    createdBy: { type: mongoose.Types.ObjectId, default: null },
    eventId: { type: mongoose.Types.ObjectId, require: true },

    name: { type: String, default: null, maxLength: 30, require: true },
    email: { type: String, maxLength: 30, lowercase: true, trim: true, default: null, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'] }, 
    phoneNumber: { type: String, maxLength: 15, default: null }, 
    
    guestsCount: { type: Number, default: 1 },
    status: { type: Number, default: 0, enum: [0, 1, 2, 3] },

    isInvited: { type: Boolean, default: true }
},
{ 
    versionKey: false,
    timestamps: true,
    strict: false
});
 
schema.index({ eventId: 1, email: 1 }, { "unique": true });
var GuestModel = mongoose.model('guest', schema)

module.exports = GuestModel;


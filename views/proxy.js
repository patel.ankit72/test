var express = require('express');
var router = express.Router();

const axios = require('axios').default
  
router.get('/', async function(request, reply) {
     
    // console.log("request.query.url ", request.query.url);
    var resheaders = {
        'Sec-Fetch-Dest': 'image',
        Pragma: 'public',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With, Etag'
    }

    const response = await axios.get(request.query.url, {
        responseType: 'stream',
        timeout: 10 * 1000
      });

    reply.set(resheaders);
    response.data.pipe(reply);
});
  

module.exports = router;
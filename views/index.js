var express = require('express');
var router = express.Router();

// api
router.use('/ecard', require('./ecard'));

router.use('/proxy', require('./proxy'));

module.exports = router;
var express = require('express');
var router = express.Router();

const { CardModel, EventModel, GuestModel } = require('../models');

const moment = require('moment');
const helpers = require('../libs/helper');
 

router.get(['/:slug', '/:slug/:guestId'], async function(req, res) {

    const cardData = await CardModel.findOneAndUpdate({ slug: req.params.slug, is_published: true }, { $inc: { hits: 1 } }).lean().exec();

    if (!cardData) {
        return res.status(404).json({ error: "page_not_found", message: "Page Not Found" })
    }

    if (cardData && cardData.is_rsvp === true) {
        const eventData = await EventModel.findOne({ cardId: cardData._id }).lean().exec();
        cardData.event = eventData;
    }


    cardData.type = cardData.type || "fold";
    cardData.orientation = cardData.orientation || "portrait";

    const defaultImage = `/public/assets/images/theme/blank_${cardData.orientation}.png`;

    const Front = cardData.files.find(i => i.place === "front");
    const Back = cardData.files.find(i => i.place === "back");
    const Left = cardData.files.find(i => i.place === "left");
    const Right = cardData.files.find(i => i.place === "right");
 
    const formattedDate = cardData.greet_date ? moment(cardData.greet_date, "DD MMM YYY").format("DD MMMM YYYY") : ""
       
    var obj = {
        Front: Front ? Front.url : defaultImage,
        Back: Back ? Back.url : defaultImage,
        Left: Left ? Left.url : defaultImage,
        Right: Right ? Right.url : defaultImage,
        Default_Image: defaultImage,
        GreetDate: formattedDate || "",
        Image_Width: cardData.orientation === "portrait" ? 1080: 2100,
        Image_Hight: cardData.orientation === "portrait" ? 1560: 1500,
        Type: cardData.type,
        Orientation: cardData.orientation,
        PageLink: `${process.env.PUBLIC_URL}/ecard/req.params.slug`,
        ContentType: cardData.content_type || "image",
        Is_RSVP: cardData.is_rsvp || false,
        Event : cardData.event || null,
        Guest: {}
    }

    if (cardData.is_rsvp && obj.Event && obj.Event.whenDate) {
        obj.Event.funcDate = moment(obj.Event.whenDate, "YYYY-MM-DD").format("dddd, Do MMMM YYYY");

        var STDate = moment(obj.Event.whenDate, "YYYY-MM-DD").format("YYYYMMDD");
        var ENDate = moment(obj.Event.endDate || obj.Event.whenDate, "YYYY-MM-DD").format("YYYYMMDD");

        obj.Event.google_calendar_link = `https://calendar.google.com/calendar/u/0/r/eventedit?text=${obj.Event.eventName}&location&dates=${STDate}/${ENDate}&sprop=website:${obj.PageLink}&sprop=name:${process.env.SITE_NAME}&details=${obj.Event.additionalInfo}`;
        obj.Event.yahoo_calendar_link = `http://calendar.yahoo.com/?v=60&view=d&type=20&title=${obj.Event.eventName}&st=${STDate}&et=${ENDate}&desc=&in_loc=${obj.Event.locationName}&url=${obj.PageLink}`;
    }

    if (cardData.is_rsvp && obj.Event) {

        const guestData = await GuestModel.find({ eventId: obj.Event._id }).lean().exec();
        obj.Guests = {
            "0": [],
            "1": [],
            "2": [],
            "3": []
        }

        if (!helpers.isEmptyOrZero(guestData)) {
           
            for (let i = 0; i < guestData.length; i++) {
                const item = guestData[i];
                
                if (typeof item.status === "number" && item.status >= 0 && item.status <=3 ) {

                    if (req.params.guestId === String(item._id)) {
                        obj.Guest = item;
                    }
                    item.current = true;
                    obj.Guests[String(item.status)].push(item);
                }
            }
        }
    }
  
    // console.log("obj.Event ", obj.Event)

  
    res.render(`ecard/${cardData.type}_card`, obj)
});

router.get(['/invites/guestlist/:eventId'], async function(req, res) {
     
    var obj = { };

    const guestData = await GuestModel.find({ eventId: req.params.eventId }).lean().exec();

    obj.Guests = {
        "0": [],
        "1": [],
        "2": [],
        "3": []
    }

    if (!helpers.isEmptyOrZero(guestData)) {
           
        for (let i = 0; i < guestData.length; i++) {
            const item = guestData[i];            
            if (typeof item.status === "number" && item.status >= 0 && item.status <=3 ) {
                obj.Guests[String(item.status)].push(item);
            }
        }
    }
  
    res.render(`ecard/guest`, obj)
});

module.exports = router;
curl --location --request POST 'https://card.replybot.me/api/event/guest/invite' \
--header 'Content-Type: application/json' \
--data-raw '{
    "eventId": "6392d1ec2e34f3b8f7ef715c",
    "guests": [
        {  
            "name":"xyz",
            "email":"zyxz@yopmail.com",
            "guestsCount":5
        }
    ]
}'
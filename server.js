
require('dotenv').config()
const express = require('express');
const app = express();

const path = require('path')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const { cors } = require('./middleware')

app.use(cookieParser())
app.use(bodyParser.json({ limit: '2mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '1mb', extended: true }));
// app.use(bodyParser.raw({ limit: '30mb', extended: true }));
app.use(cors)

app.use("/public", express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

require('./db/mongo');

// api routes
app.use('/api', require('./routes'));

// view routes
app.use('/', require('./views'));


if (!process.env.NODE_ENV) {
    throw new Error("Invalid Environment")
}

var server = app.listen(process.env.PORT, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Greeting Card App listening at http://%s:%s at %s", host, port, new Date())
});


function graceful(err) {
   console.log("graceful err:", err);
   process.exit(0);
}

process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);

process.on('uncaughtException', function (err) {
  console.log("uncaughtException: ", err);
})

process.on('unhandledRejection', (reason, p) => {   
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});

